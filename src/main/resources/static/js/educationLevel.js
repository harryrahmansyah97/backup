/** Rest API 
    1) Get Education Level
    2) Post Education Level
    3) Update Education Level
    4) Del Education Level
*/

// [Get] 1-a
function getListEducationLevel(){
	return $.ajax({
		url: "/api/educationLevel/getList",
		method: "GET",
		async: false
	});
}

// [Get] 1-b
function getEducationLevelById(id){
	return $.ajax({
		url: `/api/educationLevel/getById/${id}`,
		method: "GET",
		async: false
	});
}

// [Post] 2-x
function postNewEducationeLevel(jsonData){
	$.ajax({
		url: "/api/educationLevel/add",
		method: "POST",
		contentType: "application/json",
		data: jsonData,
		success: (resp) => {
			if(resp == "ok"){
				$("#myModal").modal("hide");
				$(".modal-body").empty();
				displayListEducationLevel();
			} else {
				$("#inpName").addClass("is-invalid");
				$(".invalid-feedback").html(resp);
			}
		}
	})
}

// [Update] 3-x
function updateEducationLevel(jsonData){
	$.ajax({
		url: "/api/educationLevel/update",
		method: "PUT",
		contentType: "application/json",
		data: jsonData,
		success: (resp) => {
			if(resp == "ok"){
				$("#myModal").modal("hide");
				$(".modal-body").empty();
				displayListEducationLevel();
			} else {
				$("#inpName").addClass("is-invalid");
				$(".invalid-feedback").html(resp);
			}
		}
	})
}

// [Del] 4-x
function deleteEducationLevel(id){
	$.ajax({
		url: `/api/educationLevel/delete/${id}`,
		method: "DELETE",
		success: (resp) => {
			if(resp == "ok"){
				$("#myModal").modal("hide");
				$(".modal-body").empty();
				displayListEducationLevel();
			} else {
				alert(resp);
			}
		}
	});
}

/** end Rest API */

/** Component */
const component = {
	inpHidden: `<input id="idLevel" type="hidden"/>`,
	inpName: `	<input id="inpName" class="form-control" placeholder="Input nama" type="text" autocomplete="off" />
				<div class="invalid-feedback">Nama Jenjang Pendidikan Tidak boleh Kosong!</div>
				`,
	delMsg: `<p id='deleteInfo'></p>`,
	
	tbodyELL: function ({id, name}, index){
		return (`
			<tr>
				<td class="col">${index + 1}</td>
					<td class="col-6">${name}</td>
					<td class="col-5">
					<div class="d-flex gap-1 justify-content-center">
						<button id="editLevel" class="btn-action px-3" value="${id}">U</button>
						<button id="deleteLevel" class="btn-action px-3" value="${id}">H</button>
					</div>
				</td>
			</tr>
		`);
	}
}
/** end Component */

/** Function */
function displayListEducationLevel() {
	$("#tbodyELL").empty();
	const dataList = getListEducationLevel().responseJSON;
	dataList.forEach((data, index) => {
		$("#tbodyELL").append(component.tbodyELL(data, index));
	});
}
/** End Function */

/** Document Ready */
$(document).ready(function(){
	displayListEducationLevel();
});
/** end Document Ready */

/** Modal */
// Modal PopUp
function displayModal(modalType){
	$("#myModal").modal("show"); // Tampilkan Modal
	$('.modal-backdrop').appendTo('.inside-modal'); // Backdrop dipindah kedalam card modal
	$(".modal-body").empty(); // Bersihkan Body Modal
	$('#submitLevel').attr('save-type', modalType || "post"); // Mengarahkan tombol save ke api
	$('#submitLevel').html("Simpan");
	$('#submitLevel').removeClass("btn-danger");
	$('#submitLevel').addClass("btn-primary");
	
	switch(modalType){
		case "update": {
			$(".modal-title").html("Ubah");
			$(".modal-body").append(component.inpHidden);
			$(".modal-body").append(component.inpName);
			break;
		}
		case "delete": {
			$(".modal-title").html("Hapus!");
			$(".modal-body").append(component.inpHidden);
			$(".modal-body").append(component.delMsg);
			$('#submitLevel').html("Hapus!");
			$('#submitLevel').removeClass("btn-primary");
			$('#submitLevel').addClass("btn-danger");
			break;
		}
		default: {
			$(".modal-title").html("Tambah");
			$(".modal-body").append(component.inpName);
			break;
		}
	}
}
/** end Modal */

/**Input Form Validation */
// Handle re-render element conflict with jquery.on
$("body").on("change keyup paste", "#inpName", function() {
	if($(this).val()==""){
		$("#inpName").addClass("is-invalid");
	} else {
		$("#inpName").removeClass("is-invalid");
	}
})

/**Input Search by name */
// Handle re-render element conflict with jquery.on
$("body").on("keyup paste", "#searchByName", function() {
	$("tbody tr").filter(function(){
		$(this).toggle($(this).text().toLowerCase().indexOf($("#searchByName").val().toLowerCase()) > -1);
	});
})

/**Tambah */
// Handle re-render element conflict with jquery.on
$("body").on("click","#addLevel", function(){
	displayModal();
});

/**Ubah Level */
// Handle re-render element conflict with jquery.on
$("body").on("click","#editLevel", function(){
	const data = getEducationLevelById($(this).val()).responseJSON;
	
	displayModal("update");
	$("#idLevel").val(data.id);
	$("#inpName").val(data.name);
});

/**Hapus Level */
// Handle re-render element conflict with jquery.on
$("body").on("click","#deleteLevel", function(){
	const data = getEducationLevelById($(this).val()).responseJSON;
	
	displayModal("delete");
	$("#idLevel").val(data.id);
	$("#deleteInfo").html(`Anda akan menghapus jenjang pendidikan ${data.name} ?`);
});

/**Action Save */
$("#submitLevel").click(function(){
	const obj = {}; // buat Object Kosong
	
	if($("#submitLevel").attr('save-type') != "post"){
		obj.id = $("#idLevel").val(); // Jika type bukan post maka ambil idLevel-nya
	}
	
	obj.name = $("#inpName").val() || ""; // Jika tidak ada #inpName maka default ""
	
	const data = JSON.stringify(obj); // Object di parsing ke bentuk JSON
	const saveType = $('#submitLevel').attr('save-type'); // Ambil nilai dari property save-type
	
	switch(saveType){
		case "post": {
			postNewEducationeLevel(data);
			break;
		}
		case "update": {
			updateEducationLevel(data);
			break;
		}
		case "delete": {
			deleteEducationLevel(obj.id);
			break;
		}
		default: {
			alert("Instruksi tidak dikenal");
			break
		}
	}
});

