package id.bootcamp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import id.bootcamp.model.M_EducationLevel;

public interface Rep_EducationLevel extends JpaRepository<M_EducationLevel, Long>{
	// Find ByName | Non Case Sensitif
	@Query(value = "SELECT COUNT(name) FROM m_education_level WHERE name ILIKE ?1 AND is_delete = false", nativeQuery = true)
	public Integer findDuplicateName(String name);
	
	// Find All Available Level (is_delete = false)
	@Query(value = "SELECT * FROM m_education_level WHERE is_delete = false", nativeQuery = true)
	public List<M_EducationLevel> findByis_DeleteFalse();
}
