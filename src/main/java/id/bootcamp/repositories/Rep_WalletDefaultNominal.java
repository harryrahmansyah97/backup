package id.bootcamp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import id.bootcamp.model.M_WalletDefaultNominal;

public interface Rep_WalletDefaultNominal extends JpaRepository<M_WalletDefaultNominal, Long>{

}
