package id.bootcamp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import id.bootcamp.model.M_Customer;

public interface Rep_Customer extends JpaRepository<M_Customer, Long>{

}
