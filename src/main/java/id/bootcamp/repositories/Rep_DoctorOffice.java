package id.bootcamp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import id.bootcamp.dto.DoctorOfficeDto;
import id.bootcamp.model.T_DoctorOffice;

public interface Rep_DoctorOffice extends JpaRepository<T_DoctorOffice, Long>{
	
	@Query(nativeQuery = true)
	public List<DoctorOfficeDto> getDoctorOfficeLocation();
}
