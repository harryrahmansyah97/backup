package id.bootcamp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.bootcamp.dto.DoctorTreatmentListDto;
import id.bootcamp.model.T_DoctorTreatment;


@Repository
public interface Rep_DoctorTreatment extends JpaRepository<T_DoctorTreatment, Long>{
	
	@Query(nativeQuery = true)
	public List<DoctorTreatmentListDto> getTreatmentList();
}
