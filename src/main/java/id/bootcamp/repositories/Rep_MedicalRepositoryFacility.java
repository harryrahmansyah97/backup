package id.bootcamp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.bootcamp.dto.MedicalFacilityCategoryDto;
import id.bootcamp.model.M_MedicalFacilityCategory;

@Repository
public interface Rep_MedicalRepositoryFacility extends JpaRepository<M_MedicalFacilityCategory, Long>{
	// Find ByName | Non Sensitive Case
	@Query(value = "SELECT COUNT(name) FROM m_medical_facility_category WHERE name ILIKE ?1 AND is_delete = false", nativeQuery = true)
	public Integer findDuplicateName( String name);
	
	@Query(nativeQuery = true)
	public List<MedicalFacilityCategoryDto> getMedicalNameAndCreated();
}
