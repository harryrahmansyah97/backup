package id.bootcamp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import id.bootcamp.dto.MedicalFacilityScheduleDto;
import id.bootcamp.model.M_MedicalFacilitySchedule;

public interface Rep_MedicalFacilitySchedule extends JpaRepository<M_MedicalFacilitySchedule, Long>{
	
	@Query(nativeQuery = true)
	public List<MedicalFacilityScheduleDto> getAllSchedule();
}
