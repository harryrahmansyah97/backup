package id.bootcamp.service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.bootcamp.dto.DoctorCurrentSpecializationDto;
import id.bootcamp.dto.DoctorDto;
import id.bootcamp.dto.DoctorExperienceDto;
import id.bootcamp.dto.DoctorOfficeDto;
import id.bootcamp.dto.DoctorTreatmentDto;
import id.bootcamp.dto.MedicalFacilityScheduleDto;
import id.bootcamp.model.FilterModel;
import id.bootcamp.repositories.Rep_Doctor;
import id.bootcamp.repositories.Rep_DoctorOffice;
import id.bootcamp.repositories.Rep_MedicalFacilitySchedule;

@Service
@Transactional
public class Svc_Doctor {

	@Autowired
	private Rep_Doctor doctor;

	@Autowired
	private Rep_DoctorOffice office;
	
	@Autowired
	private Rep_MedicalFacilitySchedule mFacilitySchedule;
	
	private List<DoctorTreatmentDto> doctorTreatment(Long id){
		return doctor.getAllDoctorAndTreatment().stream()
				.filter(data -> data.getDoctorId() == id).collect(Collectors.toList());
	}
	
	private DoctorCurrentSpecializationDto doctorCurrentSpecialization(Long id){
		return (DoctorCurrentSpecializationDto) doctor.getAllDoctorAndSpecialization().stream()
				.filter(data -> data.getDoctorId() == id).findAny().orElse(null);
	}
	
	private List<DoctorOfficeDto> doctorOffice(Long id){
		return office.getDoctorOfficeLocation().stream()
				.filter(data -> data.getDoctorId() == id && !data.getFacilityName().equalsIgnoreCase("online"))
				.limit(2).collect(Collectors.toList());
	}
	
	private DoctorExperienceDto getDoctorExperienceDto(Long id, String name){
		return (DoctorExperienceDto) doctor.getDoctorExperienceDto().stream()
				.filter(data-> data.getId() == id && data.getSpecialization().equalsIgnoreCase(name))
				.findAny().orElse(null);
	}
	
	private List<MedicalFacilityScheduleDto> getAllSchedule(Long id){
		return mFacilitySchedule.getAllSchedule().stream()
				.filter(data -> data.getDoctorId() == id)
				.collect(Collectors.toList());
	}
	
	public List<DoctorDto> getAllDoctorDto() {
		List<DoctorDto> result = doctor.getAllDoctorsDto().stream().map(s -> {
			// Doctor Treatment
			List<DoctorTreatmentDto> treatment = doctorTreatment(s.getId());
			
			// Doctor Specialization
			DoctorCurrentSpecializationDto specialization = doctorCurrentSpecialization(s.getId());
			
			// Doctor Office
			List<DoctorOfficeDto> doctorOffice = doctorOffice(s.getId());
			
			// Doctor Experience
			DoctorExperienceDto experience =  getDoctorExperienceDto(s.getId(), specialization.getName());
			
			// DoctorOnlineSchedule
			List<MedicalFacilityScheduleDto> schedule = getAllSchedule(s.getId());
			
			// Inject Kedalam Constructor
			DoctorDto dto = new DoctorDto(s, treatment, specialization, doctorOffice, experience, schedule);
			
			// Kembalikan hasil map nya kebentuk Doctor Dto
			return dto;
		}).sorted(Comparator.comparing(DoctorDto::getName, String.CASE_INSENSITIVE_ORDER)).collect(Collectors.toList());

		return result;
	}
	
	public List<DoctorDto> getAllDoctorFilter(FilterModel filter) {
		List<DoctorDto> doctor = getAllDoctorDto();
		if(filter.getLocation() != null && filter.getLocation() > 0 ) {
			doctor = doctor.stream()
					.filter(data -> {
							List<DoctorOfficeDto> listOffice = doctorOffice(data.getId()).stream()
									.filter(data2 -> data2.getLocationParentId() == filter.getLocation()).collect(Collectors.toList());
							if(listOffice.size() > 0) {
								return true;
							}
							return false;
						}
					).collect(Collectors.toList());
		}
		
		if(filter.getDoctorName() != null) {
			doctor = doctor.stream()
					.filter(s -> s.getName().toLowerCase().contains(filter.getDoctorName().toLowerCase()))
					.collect(Collectors.toList());
		}
		
		if(filter.getSpecialization() != null && filter.getSpecialization() > 0) {
			doctor = doctor.stream()
					.filter(s -> s.getSpecialization().getSpecializationId() == filter.getSpecialization())
					.collect(Collectors.toList());
		}
		
		if(filter.getTreatment() != null && !filter.getTreatment().equals("")) {
			doctor = doctor.stream()
					.filter(s -> {
						List<DoctorTreatmentDto> listTreat = doctorTreatment(s.getId()).stream()
								.filter(data -> data.getTreatment().equalsIgnoreCase(filter.getTreatment()))
								.collect(Collectors.toList());
						if(listTreat.size() > 0) {
							return true;
						}
						return false;
					}).collect(Collectors.toList());
		}
		
		return doctor;
	}

	public List<DoctorTreatmentDto> getAllDoctorAndTreatment() {
		return doctor.getAllDoctorAndTreatment();
	}

	public List<DoctorCurrentSpecializationDto> getAllDoctorAndSpecialization() {
		return doctor.getAllDoctorAndSpecialization();
	}
}
