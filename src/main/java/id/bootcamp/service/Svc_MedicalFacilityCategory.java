package id.bootcamp.service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.bootcamp.dto.MedicalFacilityCategoryDto;
import id.bootcamp.model.M_MedicalFacilityCategory;
import id.bootcamp.repositories.Rep_MedicalRepositoryFacility;

@Service
@Transactional
public class Svc_MedicalFacilityCategory {
	
	@Autowired
	private Rep_MedicalRepositoryFacility repFacility;
	
	public M_MedicalFacilityCategory getById(Long id) {
		return repFacility.findById(id).get();
	}
	
	public List<MedicalFacilityCategoryDto> getList(){
		List<MedicalFacilityCategoryDto> list = repFacility.getMedicalNameAndCreated().stream()
				.filter(facility -> facility.getIsDelete() == false)
				.sorted(Comparator.comparing(MedicalFacilityCategoryDto::getName, String.CASE_INSENSITIVE_ORDER))
				.collect(Collectors.toList());
		
		return list;
	}
	
	public Boolean isDuplicateName(String name) {
		if(repFacility.findDuplicateName(name) != 0) {
			return true;
		}
		return false;
	}
	
	public Boolean isIdValid(Long id) {
		if(repFacility.findById(id).orElse(null) == null) {
			return false;
		}
		
		return true;
	}
	
	public void saveLevel(M_MedicalFacilityCategory model) {
		repFacility.save(model);
	}
}
