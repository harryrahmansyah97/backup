package id.bootcamp.service;

import java.util.List;

import id.bootcamp.model.M_EducationLevel;

public interface Svc_EducationLevel {
	
	public M_EducationLevel getById(Long id);
	public List<M_EducationLevel> getList();
	public Boolean isDuplicateName(String name);
	public Boolean isIdValid(Long id);
	public void saveLevel(M_EducationLevel model);
}
