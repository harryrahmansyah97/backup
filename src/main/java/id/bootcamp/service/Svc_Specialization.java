package id.bootcamp.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.bootcamp.dto.DoctorCurrentSpecializationDto;
import id.bootcamp.model.M_Specialization;
import id.bootcamp.repositories.Rep_Specialization;

@Service
@Transactional
public class Svc_Specialization {
	
	@Autowired
	private Rep_Specialization repSpecialization;
	
	public List<M_Specialization> getAllSpecialization(){
		return repSpecialization.findAll();
	}
	
	public List<DoctorCurrentSpecializationDto> getAllDoctorAndSpecializationList(){
		return repSpecialization.getAllDoctorAndSpecializationList();
	}
}
