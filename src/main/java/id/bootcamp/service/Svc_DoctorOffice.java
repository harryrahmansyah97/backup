package id.bootcamp.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.bootcamp.dto.DoctorOfficeDto;
import id.bootcamp.repositories.Rep_DoctorOffice;

@Service
@Transactional
public class Svc_DoctorOffice {

	@Autowired
	private Rep_DoctorOffice repDoctorOffice;
	
	public List<DoctorOfficeDto> getAllDoctorOffice(){
		return repDoctorOffice.getDoctorOfficeLocation();
	}
}
