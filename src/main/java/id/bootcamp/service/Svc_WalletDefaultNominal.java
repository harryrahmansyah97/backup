package id.bootcamp.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.bootcamp.model.M_WalletDefaultNominal;
import id.bootcamp.repositories.Rep_WalletDefaultNominal;

@Service
@Transactional
public class Svc_WalletDefaultNominal {

	@Autowired
	private Rep_WalletDefaultNominal defaultNominal;
	
	public List<M_WalletDefaultNominal> getAllNominal(){
		return defaultNominal.findAll();
	}
}
