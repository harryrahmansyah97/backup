package id.bootcamp.service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import id.bootcamp.model.M_EducationLevel;
import id.bootcamp.repositories.Rep_EducationLevel;

@Transactional
public class SvcImpl_EducationLevel implements Svc_EducationLevel {

	private Rep_EducationLevel repository;

	public SvcImpl_EducationLevel(Rep_EducationLevel repo) {
		this.repository = repo;
	}

	@Override
	public M_EducationLevel getById(Long id) {
		return repository.findById(id).get();
	}

	@Override
	public List<M_EducationLevel> getList() {
		List<M_EducationLevel> orderAsc = repository.findAll().stream()
				.filter(education -> education.getIsDelete() == false)
				.sorted(Comparator.comparing(M_EducationLevel::getName, String.CASE_INSENSITIVE_ORDER))
				.collect(Collectors.toList());
		return orderAsc;
	}

	@Override
	public Boolean isDuplicateName(String name) {
		if (repository.findDuplicateName(name) != 0) {
			return true;
		}
		return false;
	}

	@Override
	public Boolean isIdValid(Long id) {
		if (repository.findById(id).orElse(null) == null) {
			return false;
		}
		return true;
	}

	@Override
	public void saveLevel(M_EducationLevel model) {
		repository.save(model);

	}
	
}
