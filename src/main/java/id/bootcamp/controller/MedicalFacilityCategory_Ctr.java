package id.bootcamp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("medical")
public class MedicalFacilityCategory_Ctr {
	
	@RequestMapping("category-facility")
	public String category(Model model) {
		return "medical-facility-category/medical-facility-category";
	}
}
