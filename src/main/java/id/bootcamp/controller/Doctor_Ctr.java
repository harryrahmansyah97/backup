package id.bootcamp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("doctor")
public class Doctor_Ctr {
	
	@RequestMapping("find-doctor")
	public String findDoctor() {
		return "doctor/find_doctor";
	}
}
