package id.bootcamp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.bootcamp.dto.DoctorOfficeDto;
import id.bootcamp.service.Svc_DoctorOffice;

@RestController
@RequestMapping("api/doctorOffice")
public class DoctorOffice_ResCtr {

	@Autowired
	private Svc_DoctorOffice doctorOffice;
	
	@RequestMapping("getList")
	public List<DoctorOfficeDto> getAllDoctorOfficeList(){
		return doctorOffice.getAllDoctorOffice();
	}
}
