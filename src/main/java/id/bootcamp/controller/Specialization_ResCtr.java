package id.bootcamp.controller;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.bootcamp.dto.DoctorCurrentSpecializationDto;
import id.bootcamp.model.M_Specialization;
import id.bootcamp.service.Svc_Specialization;

@RestController
@RequestMapping("api/specialization")
public class Specialization_ResCtr {
	
	@Autowired
	private Svc_Specialization svcSpecialization;
	
	@RequestMapping("getList")
	public List<M_Specialization> getList(){
		return svcSpecialization.getAllSpecialization().stream()
				.filter(data -> data.getIsDelete() == false)
				.sorted(Comparator.comparing(M_Specialization::getName, String.CASE_INSENSITIVE_ORDER))
				.collect(Collectors.toList());
	}
	
	@RequestMapping("getAllDoctorAndSpecializationList")
	public List<DoctorCurrentSpecializationDto> getAllDoctorAndSpecializationList(){
		return svcSpecialization.getAllDoctorAndSpecializationList();
	}
}
