package id.bootcamp.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.bootcamp.dto.MedicalFacilityCategoryDto;
import id.bootcamp.model.M_MedicalFacilityCategory;
import id.bootcamp.service.Svc_MedicalFacilityCategory;

@RestController
@RequestMapping("api/medicalFacilityCategory")
public class MedicalFacilityCategory_ResCtr {

	@Autowired
	private Svc_MedicalFacilityCategory svcFacilityCategory;

	// Get List
	@GetMapping("getList")
	public List<MedicalFacilityCategoryDto> getList() {
		return svcFacilityCategory.getList();
	}

	@GetMapping("getById/{id}")
	public M_MedicalFacilityCategory getById(@PathVariable("id") Long id) {
		return svcFacilityCategory.getById(id);
	}

	@PostMapping("add")
	public String add(@RequestBody M_MedicalFacilityCategory data) {
		// Validasi name bukan string kosong
		String strName = data.getName().trim();
		if (strName.equals("")) {
			return "Nama Tidak Boleh Kosong";
		}

		// Validasi duplikat name | CaseSensitif Aktif
		if (svcFacilityCategory.isDuplicateName(strName)) {
			return "Nama Yang Sama Sudah Tersedia";
		}

		data.setCreatedBy(1L);
		data.setCreatedOn(new Date());

		svcFacilityCategory.saveLevel(data);

		return "ok";
	}

	@PutMapping("update")
	public String update(@RequestBody M_MedicalFacilityCategory data) {
		// Validasi id
		Long dataID = data.getId();
		if (!svcFacilityCategory.isIdValid(dataID)) {
			return "invalid request";
		}

		// Validasi name bukan string kosong
		String strName = data.getName().trim();
		if (strName.equals("")) {
			return "Nama Tidak Boleh Kosong";
		}

		M_MedicalFacilityCategory oldData = svcFacilityCategory.getById(dataID);
		// Validasi jika new name != old name
		if (!oldData.getName().equals(data.getName())) {
			// Validasi duplikat name | CaseSensitif Aktif
			if (svcFacilityCategory.isDuplicateName(strName)) {
				return "Nama Yang Sama Sudah Tersedia";
			}
		}

		data.setCreatedBy(oldData.getCreatedBy());
		data.setCreatedOn(oldData.getCreatedOn());

		data.setModifiedBy(1L);
		data.setModifiedOn(new Date());

		svcFacilityCategory.saveLevel(data);

		return "ok";
	}
	
	@DeleteMapping("delete/{id}")
	public String delete(@PathVariable("id") Long id) {
		
		//Validasi ID yang akan dihapus
		Boolean isValid = svcFacilityCategory.isIdValid(id);
		if(!isValid) {
			return "404 - id tidak valid";
		}
		
		M_MedicalFacilityCategory data = svcFacilityCategory.getById(id);
		data.setDeletedBy(1L);
		data.setDeletedOn(new Date());
		data.setIsDelete(true);
		
		svcFacilityCategory.saveLevel(data);
		return "ok";
	}

}
