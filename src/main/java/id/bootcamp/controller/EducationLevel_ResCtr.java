package id.bootcamp.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.bootcamp.model.M_EducationLevel;
import id.bootcamp.service.Svc_EducationLevel;

@RestController
@RequestMapping("api/educationLevel")
public class EducationLevel_ResCtr {

	@Autowired
	private Svc_EducationLevel service;

	// Get List
	@GetMapping("getList")
	public List<M_EducationLevel> getList() {
		return service.getList();
	}
	
	@GetMapping("getById/{id}")
	public M_EducationLevel getById(@PathVariable("id") Long id) {
		return service.getById(id);
	}

	@PostMapping("add")
	public String add(@RequestBody M_EducationLevel data) {
		// Validasi name bukan string kosong
		String strName = data.getName();
		if (strName.equals("")) {
			return "Nama Tidak Boleh Kosong";
		}

		// Validasi duplikat name | CaseSensitif Aktif
		if (service.isDuplicateName(strName)) {
			return "Nama Yang Sama Sudah Tersedia";
		}

		data.setCreatedBy(1L);
		data.setCreatedOn(new Date());

		service.saveLevel(data);
		return "ok";
	}

	@PutMapping("update")
	public String update(@RequestBody M_EducationLevel data) {
		// Validasi id
		Long dataID = data.getId();
		if(!service.isIdValid(dataID)) {
			return "invalid request";
		}
		
		// Validasi name bukan string kosong
		String strName = data.getName();
		if (strName.equals("")) {
			return "Nama Tidak Boleh Kosong";
		}
		
		M_EducationLevel oldData = service.getById(dataID);
		// Validasi jika new name != old name
		if(!oldData.getName().equals(data.getName())) {
			// Validasi duplikat name | CaseSensitif Aktif
			if (service.isDuplicateName(strName)) {
				return "Nama Yang Sama Sudah Tersedia";
			}
		}
		
		data.setCreatedBy(oldData.getCreatedBy());
		data.setCreatedOn(oldData.getCreatedOn());
		
		data.setModifiedBy(1L);
		data.setModifiedOn(new Date());

		service.saveLevel(data);
		return "ok";
	}
	
	@DeleteMapping("delete/{id}")
	public String delete(@PathVariable("id") Long id) {
		
		//Validasi ID yang akan dihapus
		Boolean isValid = service.isIdValid(id);
		if(!isValid) {
			return "404 - id tidak valid";
		}
		
		M_EducationLevel data = service.getById(id);
		data.setDeletedBy(1L);
		data.setDeletedOn(new Date());
		data.setIsDelete(true);
		
		service.saveLevel(data);
		return "ok";
	}

}
