package id.bootcamp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.bootcamp.dto.DoctorCurrentSpecializationDto;
import id.bootcamp.dto.DoctorDto;
import id.bootcamp.dto.DoctorTreatmentDto;
import id.bootcamp.dto.MedicalFacilityScheduleDto;
import id.bootcamp.model.FilterModel;
import id.bootcamp.service.Svc_Doctor;

@RestController
@RequestMapping("api/doctor")
public class Doctor_ResCtr {

	@Autowired
	private Svc_Doctor service;

	@RequestMapping("getList")
	public List<DoctorDto> getAllDoctorDto() {
		return service.getAllDoctorDto();
	}

	@RequestMapping("getListTreatment")
	public List<DoctorTreatmentDto> getAllDoctorAndTreatment() {
		return service.getAllDoctorAndTreatment();
	}

	@RequestMapping("getListCurrentSpecialization")
	public List<DoctorCurrentSpecializationDto> getAllDoctorAndSpecialization() {
		return service.getAllDoctorAndSpecialization();
	}
	
	@PostMapping("byFilter")
	public List<DoctorDto> getByLocation(@RequestBody FilterModel model){
		System.out.println("nama : "+model.getDoctorName());
		return service.getAllDoctorFilter(model);
	}
	
//	@GetMapping("facilitySchedule")
//	public List<MedicalFacilityScheduleDto> getAllSchedule(){
//		return service.getAllSchedule();
//	}
}
