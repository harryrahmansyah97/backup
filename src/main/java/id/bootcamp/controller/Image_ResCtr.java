package id.bootcamp.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("api/image")
public class Image_ResCtr {
	
	// public static String fileDownloadedPath = "D:\\xsis_bootcamp\\297-a-mini-project\\src\\main\\resources\\static\\";
	
	@GetMapping("{name}")
	public String getImage(@PathVariable("name") String name) {
		String showImageUrl = ServletUriComponentsBuilder
							 .fromCurrentContextPath()
							 .path("/img/")
							 .path(name)
							 .toUriString();
		return showImageUrl;
	}
}
