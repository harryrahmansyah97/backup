package id.bootcamp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("education")
public class EducationLevel_Ctr {
	
	@RequestMapping("/level")
	public String category(Model model) {
		model.addAttribute("title", "Education Level");
		return "education/education_level.html";
	}
}
