package id.bootcamp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("wallet")
public class Wallet_Ctr {

	@GetMapping("withdraw")
	public String walletWithdraw() {
		return "wallet-withdraw/withdraw";
	}
}
