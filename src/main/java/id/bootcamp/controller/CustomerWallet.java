package id.bootcamp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.bootcamp.model.M_WalletDefaultNominal;
import id.bootcamp.service.Svc_WalletDefaultNominal;

@RestController
@RequestMapping("api/wallet")
public class CustomerWallet {

	@Autowired
	private Svc_WalletDefaultNominal defaultNominal;
	
	@GetMapping("defaultNominal")
	public List<M_WalletDefaultNominal> getWalletDefaultNominal(){
		return defaultNominal.getAllNominal();
	}
}
