package id.bootcamp.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import id.bootcamp.repositories.Rep_EducationLevel;
import id.bootcamp.service.Svc_EducationLevel;
import id.bootcamp.service.SvcImpl_EducationLevel;

@Configuration
public class BeanConfiguration {
	
	@Bean
	Svc_EducationLevel educationLevelService(Rep_EducationLevel repository) {
		return new SvcImpl_EducationLevel(repository);
	}
}
