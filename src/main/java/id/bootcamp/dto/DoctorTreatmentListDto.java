package id.bootcamp.dto;

public class DoctorTreatmentListDto {
	
	private Long id;
	private String name;
	
	public DoctorTreatmentListDto() {
		// TODO Auto-generated constructor stub
	}

	public DoctorTreatmentListDto(Long id, String name) {
		this.id = id;
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	
}
