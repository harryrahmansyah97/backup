package id.bootcamp.dto;

public class MedicalFacilityCategoryDto {
	private Long id;
	private String name;
	private String createdBy;
	private Boolean isDelete;
	
	public MedicalFacilityCategoryDto() {
		// TODO Auto-generated constructor stub
	}

	public MedicalFacilityCategoryDto(Long id, String name, String createdBy, Boolean isDelete) {
		super();
		this.id = id;
		this.name = name;
		this.createdBy = createdBy;
		this.isDelete = isDelete;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
	
}
