package id.bootcamp.dto;

public class DoctorCurrentSpecializationDto {
	private Long id;
	private Long doctorId;
	private Long specializationId;
	private String name;
	private Boolean isDelete;
	
	public DoctorCurrentSpecializationDto() {
		// TODO Auto-generated constructor stub
	}

	public DoctorCurrentSpecializationDto(Long id, Long doctorId, Long specializationId, String name,
			Boolean isDelete) {
		super();
		this.id = id;
		this.doctorId = doctorId;
		this.specializationId = specializationId;
		this.name = name;
		this.isDelete = isDelete;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(Long doctorId) {
		this.doctorId = doctorId;
	}

	public Long getSpecializationId() {
		return specializationId;
	}

	public void setSpecializationId(Long specializationId) {
		this.specializationId = specializationId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
	
}
