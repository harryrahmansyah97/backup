package id.bootcamp.dto;

public class MedicalFacilityScheduleDto {
	
	private Long doctorId;
	private Long officeScheduleId;
	private Long medicalFacilityScheduleId;
	private String day;
	private String timeStartSchedule;
	private String timeEndSchedule;
	private Long medicalFacilityId;
	private String facilityName;
	
	public MedicalFacilityScheduleDto() {
		// TODO Auto-generated constructor stub
	}

	public MedicalFacilityScheduleDto(Long doctorId, Long officeScheduleId, Long medicalFacilityScheduleId, String day,
			String timeStartSchedule, String timeEndSchedule, Long medicalFacilityId, String facilityName) {
		super();
		this.doctorId = doctorId;
		this.officeScheduleId = officeScheduleId;
		this.medicalFacilityScheduleId = medicalFacilityScheduleId;
		this.day = day;
		this.timeStartSchedule = timeStartSchedule;
		this.timeEndSchedule = timeEndSchedule;
		this.medicalFacilityId = medicalFacilityId;
		this.facilityName = facilityName;
	}

	public Long getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(Long doctorId) {
		this.doctorId = doctorId;
	}

	public Long getOfficeScheduleId() {
		return officeScheduleId;
	}

	public void setOfficeScheduleId(Long officeScheduleId) {
		this.officeScheduleId = officeScheduleId;
	}

	public Long getMedicalFacilityScheduleId() {
		return medicalFacilityScheduleId;
	}

	public void setMedicalFacilityScheduleId(Long medicalFacilityScheduleId) {
		this.medicalFacilityScheduleId = medicalFacilityScheduleId;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getTimeStartSchedule() {
		return timeStartSchedule;
	}

	public void setTimeStartSchedule(String timeStartSchedule) {
		this.timeStartSchedule = timeStartSchedule;
	}

	public String getTimeEndSchedule() {
		return timeEndSchedule;
	}

	public void setTimeEndSchedule(String timeEndSchedule) {
		this.timeEndSchedule = timeEndSchedule;
	}

	public Long getMedicalFacilityId() {
		return medicalFacilityId;
	}

	public void setMedicalFacilityId(Long medicalFacilityId) {
		this.medicalFacilityId = medicalFacilityId;
	}

	public String getFacilityName() {
		return facilityName;
	}

	public void setFacilityName(String facilityName) {
		this.facilityName = facilityName;
	}
	
}
