package id.bootcamp.dto;

public class DoctorOfficeDto {
	
	private Long id;
	private Long doctorId;
	private Long medicalFacilityId;
	private String facilityName;
	private Long locationId;
	private String locationName;
	private Long locationParentId;
	private String locationParentName;
	private String facilitySpecialization;
	
	public DoctorOfficeDto() {
		// TODO Auto-generated constructor stub
	}

	public DoctorOfficeDto(Long id, Long doctorId, Long medicalFacilityId, String facilityName, Long locationId,
			String locationName, Long locationParentId, String locationParentName, String facilitySpecialization) {
		super();
		this.id = id;
		this.doctorId = doctorId;
		this.medicalFacilityId = medicalFacilityId;
		this.facilityName = facilityName;
		this.locationId = locationId;
		this.locationName = locationName;
		this.locationParentId = locationParentId;
		this.locationParentName = locationParentName;
		this.facilitySpecialization = facilitySpecialization;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(Long doctorId) {
		this.doctorId = doctorId;
	}

	public Long getMedicalFacilityId() {
		return medicalFacilityId;
	}

	public void setMedicalFacilityId(Long medicalFacilityId) {
		this.medicalFacilityId = medicalFacilityId;
	}

	public String getFacilityName() {
		return facilityName;
	}

	public void setFacilityName(String facilityName) {
		this.facilityName = facilityName;
	}

	public Long getLocationId() {
		return locationId;
	}

	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public Long getLocationParentId() {
		return locationParentId;
	}

	public void setLocationParentId(Long locationParentId) {
		this.locationParentId = locationParentId;
	}

	public String getLocationParentName() {
		return locationParentName;
	}

	public void setLocationParentName(String locationParentName) {
		this.locationParentName = locationParentName;
	}

	public String getFacilitySpecialization() {
		return facilitySpecialization;
	}

	public void setFacilitySpecialization(String facilitySpecialization) {
		this.facilitySpecialization = facilitySpecialization;
	}
	
}
