package id.bootcamp.dto;

public class DoctorExperienceDto {
	private Long id;
	private String specialization;
	private Integer yearsExperience;
	
	public DoctorExperienceDto() {
		// TODO Auto-generated constructor stub
	}
	
	public DoctorExperienceDto(Long id, String specialization, Integer yearsExperience) {
		super();
		this.id = id;
		this.specialization = specialization;
		this.yearsExperience = yearsExperience;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSpecialization() {
		return specialization;
	}

	public void setSpecialization(String specialization) {
		this.specialization = specialization;
	}

	public Integer getYearsExperience() {
		return yearsExperience;
	}

	public void setYearsExperience(Integer yearsExperience) {
		this.yearsExperience = yearsExperience;
	}
	
	
}
