package id.bootcamp.model;

public class FilterModel {
	private Long location;
	private String doctorName;
	private Long specialization;
	private String treatment;

	public Long getLocation() {
		return location;
	}

	public void setLocation(Long location) {
		this.location = location;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

	public Long getSpecialization() {
		return specialization;
	}

	public void setSpecialization(Long specialization) {
		this.specialization = specialization;
	}

	public String getTreatment() {
		return treatment;
	}

	public void setTreatment(String treatment) {
		this.treatment = treatment;
	}

}
