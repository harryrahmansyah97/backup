package id.bootcamp.model;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

import org.hibernate.annotations.NamedNativeQuery;

import id.bootcamp.dto.MedicalFacilityScheduleDto;

@SqlResultSetMapping(
	name = "facilityScheduleMapping",
	classes = {
		@ConstructorResult(
			targetClass = MedicalFacilityScheduleDto.class,
			columns = {
				@ColumnResult(name = "doctor_id", type = Long.class),
				@ColumnResult(name = "office_schedule_id", type = Long.class),
				@ColumnResult(name = "medical_facility_schedule_id", type = Long.class),
				@ColumnResult(name = "day"),
				@ColumnResult(name = "time_schedule_start"),
				@ColumnResult(name = "time_schedule_end"),
				@ColumnResult(name = "medical_facility_id", type = Long.class),
				@ColumnResult(name = "facility_name")
			}
		)
	}
)

@NamedNativeQuery(
	name = "M_MedicalFacilitySchedule.getAllSchedule",
	query = "SELECT "
			+ "	dc.id as doctor_id, "
			+ "	dos.id as office_schedule_id, "
			+ "	dos.medical_facility_schedule_id, "
			+ "	mfs.day, "
			+ "	mfs.time_schedule_start, "
			+ "	mfs.time_schedule_end, "
			+ "	ms.id as medical_facility_id, "
			+ "	ms.name as facility_name "
			+ " FROM m_doctor dc "
			+ " JOIN t_doctor_office_schedule dos "
			+ "	ON dos.doctor_id = dc.id "
			+ " JOIN m_medical_facility_schedule mfs "
			+ "	ON dos.medical_facility_schedule_id = mfs.id "
			+ " JOIN m_medical_facility ms "
			+ "	ON mfs.medical_facility_id = ms.id",
	resultSetMapping = "facilityScheduleMapping",
	resultClass = MedicalFacilityScheduleDto.class
)


@Entity
@Table(name = "m_medical_facility_schedule")
public class M_MedicalFacilitySchedule extends BaseProperties{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false)
	private Long id;
	
	private Long medical_facility_id;
	
	@Column(length = 10)
	private String day;
	
	@Column(length = 10)
	private String time_schedule_start;
	
	@Column(length = 10)
	private String time_schedule_end;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getMedical_facility_id() {
		return medical_facility_id;
	}

	public void setMedical_facility_id(Long medical_facility_id) {
		this.medical_facility_id = medical_facility_id;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getTime_schedule_start() {
		return time_schedule_start;
	}

	public void setTime_schedule_start(String time_schedule_start) {
		this.time_schedule_start = time_schedule_start;
	}

	public String getTime_schedule_end() {
		return time_schedule_end;
	}

	public void setTime_schedule_end(String time_schedule_end) {
		this.time_schedule_end = time_schedule_end;
	}
	
	
}
