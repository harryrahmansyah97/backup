package id.bootcamp.model;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

import id.bootcamp.dto.DoctorOfficeDto;

@SqlResultSetMapping(
	name = "doctorOfficeMapping",
	classes = {
		@ConstructorResult(
			targetClass = DoctorOfficeDto.class,
			columns = {
				@ColumnResult(name = "id", type = Long.class),
				@ColumnResult(name = "doctor_id", type = Long.class),
				@ColumnResult(name = "medical_facility_id", type = Long.class),
				@ColumnResult(name = "name"),
				@ColumnResult(name = "location_id", type = Long.class),
				@ColumnResult(name = "location_name"),
				@ColumnResult(name = "location_parent_id", type = Long.class),
				@ColumnResult(name = "location_parent_name"),
				@ColumnResult(name = "specialization")
				
			}
		)
	}
)

@NamedNativeQuery(
	name = "T_DoctorOffice.getDoctorOfficeLocation",
	query = "SELECT "
			+ "dco.id, dco.doctor_id, dco.medical_facility_id, "
			+ "mf.name, mf.location_id, "
			+ "loc.name as location_name, "
			+ "loc.parent_id as location_parent_id, locb.name as location_parent_name, "
			+ "dco.specialization "
			+ "FROM m_biodata bd "
			+ "RIGHT JOIN m_doctor dc ON dc.biodata_id = bd.id "
			+ "RIGHT JOIN t_doctor_office dco ON dco.doctor_id = dc.id "
			+ "LEFT JOIN m_medical_facility mf ON dco.medical_facility_id = mf.id "
			+ "LEFT JOIN m_location loc ON mf.location_id = loc.id "
			+ "LEFT JOIN m_location locb ON loc.parent_id = locb.id",
	resultSetMapping = "doctorOfficeMapping",
	resultClass = DoctorOfficeDto.class
)


@Entity
@Table(name = "t_doctor_office")
public class T_DoctorOffice extends BaseProperties {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false)
	private Long id;

	private Long doctor_id;

	private Long medical_facility_id;

	@Column(length = 100)
	private String specialization;

	public T_DoctorOffice() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDoctor_id() {
		return doctor_id;
	}

	public void setDoctor_id(Long doctor_id) {
		this.doctor_id = doctor_id;
	}

	public Long getMedical_facility_id() {
		return medical_facility_id;
	}

	public void setMedical_facility_id(Long medical_facility_id) {
		this.medical_facility_id = medical_facility_id;
	}

	public String getSpecialization() {
		return specialization;
	}

	public void setSpecialization(String specialization) {
		this.specialization = specialization;
	}

}
