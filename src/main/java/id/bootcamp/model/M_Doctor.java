package id.bootcamp.model;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

import org.hibernate.annotations.NamedNativeQuery;

import id.bootcamp.dto.DoctorExperienceDto;

@SqlResultSetMapping(
	name = "experience",
	classes = {
		@ConstructorResult(
			targetClass = DoctorExperienceDto.class,
			columns = {
				@ColumnResult(name = "id", type = Long.class),
				@ColumnResult(name = "specialization"),
				@ColumnResult(name = "years_experience", type = Integer.class)
			}
		)
	}
)

@NamedNativeQuery(
	name = "M_Doctor.getDoctorExperienceDto",
	query = "SELECT dc.id, dof.specialization, min(date_part('year', AGE(dof.created_on))) years_experience "
			+ "FROM m_doctor dc "
			+ "JOIN t_doctor_office dof ON dof.doctor_id = dc.id "
			+ "GROUP BY dc.id, specialization",
	resultSetMapping = "experience",
	resultClass = DoctorExperienceDto.class
)


@Entity
@Table(name = "m_doctor")
public class M_Doctor extends BaseProperties {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false)
	private Long id;

	private Long biodata_id;

	@Column(length = 50)
	private String str;

	public M_Doctor() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getBiodata_id() {
		return biodata_id;
	}

	public void setBiodata_id(Long biodata_id) {
		this.biodata_id = biodata_id;
	}

	public String getStr() {
		return str;
	}

	public void setStr(String str) {
		this.str = str;
	}

}
