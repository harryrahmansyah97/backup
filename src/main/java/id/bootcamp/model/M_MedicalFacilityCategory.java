package id.bootcamp.model;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

import org.hibernate.annotations.NamedNativeQuery;

import id.bootcamp.dto.DoctorExperienceDto;
import id.bootcamp.dto.MedicalFacilityCategoryDto;

@SqlResultSetMapping(
	name = "newMapping",
	classes = {
			@ConstructorResult(
				targetClass = MedicalFacilityCategoryDto.class,
				columns = {
					@ColumnResult(name = "id", type = Long.class),
					@ColumnResult(name = "facility"),
					@ColumnResult(name = "created_by"),
					@ColumnResult(name = "is_delete", type = Boolean.class)
				}
			)
		}
)

@NamedNativeQuery(
	name = "M_MedicalFacilityCategory.getMedicalNameAndCreated",
	query = "SELECT "
			+ "	mfc.id, "
			+ "	mfc.name as facility, "
			+ "	bd.fullname as created_by,"
			+ " mfc.is_delete "
			+ "FROM m_medical_facility_category mfc "
			+ "LEFT JOIN m_user us "
			+ "	ON us.id = mfc.created_by "
			+ "LEFT JOIN m_biodata bd "
			+ "	ON us.biodata_id = bd.id;",
	resultSetMapping = "newMapping",
	resultClass = MedicalFacilityCategoryDto.class
)


@Entity
@Table(name = "m_medical_facility_category")
public class M_MedicalFacilityCategory extends BaseProperties{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false)
	private Long id;

	@Column(length = 50)
	private String name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
